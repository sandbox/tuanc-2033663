(function($){
  //Add the tester invoke function to submit buttons
  var htmlpurifier_timeout_prevention_invoke = function(form) {
    //grab all textareas in the form
    var textareas = form.find('.text-format-wrapper').find('.form-textarea');
    //If using ckeditor
    if (typeof CKEDITOR != 'undefined' && !jQuery.isEmptyObject(CKEDITOR.instances) && textareas.length != 0){
      var test = true;
      textareas.each(function() {
        if (test == true) {
          $.ajax({
            url: "/htmlpurifier_timeout_prevention",

            data: {
              text: CKEDITOR.instances[this.id].getData(),
              input_format: $(this).parents('.text-format-wrapper').find('.filter-list').val()
            },
            timeout: 10000,
            type: 'POST',
            async: false,
            success: function(data, textStatus, jqXHR){

              //If JSON is returned, means passed the test
              var is_json = true;
              try{
                $.parseJSON(data);
              } catch (e) {
                is_json = false;
              }

              if(!is_json){
                $('.node-form').first().before('<div class="messages error">Form did not submit. Your html might be too complicated to process.</div>');
                test = false;
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
              if (textStatus == "timeout"){
                $('.node-form').first().before('<div class="messages error">Form did not submit. Your html might be too complicated to process.</div>');
                test = false;
              }
            }
          });
        }

      });
      return test;
    }

    //If not using plain text editor instead of ckeditor
    if (textareas.length != 0){
      var test = true;
      textareas.each(function() {
        if (test == true) {
          // @todo See htmlpurifier_timeout_prevention_form_alter() in .module.
          $.ajax({
            url: '/htmlpurifier_timeout_prevention',
            data: {
              text: this.value,
              input_format: $(this).parents('.text-format-wrapper').find('.filter-list').val()
            },
            timeout: 10000,
            type: 'POST',
            async: false,
            success: function(data, textStatus, jqXHR){

              //If JSON is returned, means passed the test
              var is_json = true;
              try{
                $.parseJSON(data);
              } catch (e) {
                is_json = false;
              }

              if(!is_json){
                $('.node-form').first().before('<div class="messages error">Form did not submit. Your html might be too complicated to process.</div>');
                test = false;
              }
            },
            error: function(jqXHR, textStatus, errorThrown){
              if (textStatus == "timeout"){
                $('.node-form').first().before('<div class="messages error">Form did not submit. Your html might be too complicated to process.</div>');
                test = false;
              }
            }

          });
        }

      });
      return test;

    }
  };
  Drupal.behaviors.htmlpurifier_timeout_prevention_register = {
    attach: function (context, settings) {
      try {
        // @todo These submit buttons should be findable in some other form than
        //   by ID. Maybe set on the whole form's submit handler, and check for
        //   op != 'Cancel'.
        $('#edit-preview, #edit-publish, #edit-submit').click(function(){
          return htmlpurifier_timeout_prevention_invoke($(this.form));
        });
      } catch(e) { }
    }
  }

})(jQuery);
