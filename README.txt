Description
-------------
A page's HTML content may be too complicated for HTMLPurifier to process within
PHP's max execution time limit, potentially causing a PHP fatal error during
node submission. This module uses AJAX at node form submission to test
HTMLPurifier execution time to prevent this from happening.

 1. When a user submits the form, an AJAX request sends all filtered text field
    (typically "body" field) content to an AJAX listener.
 2. The AJAX listener temporarily reduces PHP's max_execution_time (10 seconds
    by default) and invokes HTMLPurifier by calling check_markup().
 3. If HTMLPurifier completes processing within the reduced time frame, the
    user's browser will receive a successful JSON response and will allow the
    form to submit. Otherwise, the form is prevented from submitting and an
    error message pops up to inform the user that the content is too
    complicated.

Dependencies
-------------
htmlpurifier

Author
-------------
Chiao-Yu Tuan
Jay Dansand
Lawrence University
